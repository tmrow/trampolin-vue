$(document).ready(function() {
    if ($(window).width()<992) {
        $("div.clickeable").on('click touchstart', function () {
            window.location = $(this).data("link");
            return false;
        });
    }
      $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4'
      });
});
