// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueCarousel from 'vue-carousel'
import BootstrapVue from 'bootstrap-vue'

import router from './router'

Vue.use(VueCarousel)
Vue.use(BootstrapVue)

Vue.config.productionTip = false

let globalData = new Vue({
  data: {
    $login: false,
    // obra / sala
    $user: 'obra',
    $tipo: 'teatro'
  }
})

Vue.mixin({
  computed: {
    $login: {
      get: function () { return globalData.$data.$login },
      set: function (Logged) { globalData.$data.$login = Logged }
    },
    $user: {
      get: function () { return globalData.$data.$user },
      set: function (Obra) { globalData.$data.$user = Obra }
    },
    $tipo: {
      get: function () { return globalData.$data.$tipo },
      set: function (Tipo) { globalData.$data.$tipo = Tipo }
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App, VueCarousel },
  template: '<App/>'
})
