import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      name: 'Login',
      path: '/',
      component: () => import('@/views/Login')
    },
    {
      name: 'Home',
      path: '/home',
      component: () => import('@/views/Home')
    },
    {
      name: 'Registro',
      path: '/registro',
      component: () => import('@/views/Registro')
    },
    {
      name: 'Registro de espectaculo',
      path: '/registro/espectaculo',
      component: () => import('@/views/Registro/RegistroEspectaculo')
    },
    {
      name: 'Perfil',
      path: '/perfil',
      component: () => import('@/views/Perfil')
    },
    {
      name: 'Perfil',
      path: '/perfil/ok',
      component: () => import('@/views/Perfil/PerfilCompleto')
    },
    {
      name: 'Perfil',
      path: '/perfil/editar',
      component: () => import('@/views/EspectaculoEdit')
    },
    {
      name: 'Agenda obra',
      path: '/perfil/agenda',
      component: () => import('@/views/Perfil/PerfilAgenda')
    },
    {
      name: 'Cachet obra',
      path: '/perfil/cachet',
      component: () => import('@/views/Perfil/PerfilCachet')
    },
    {
      name: 'Ficha Obra',
      path: '/perfil/ficha',
      component: () => import('@/views/Perfil/PerfilFicha')
    },
    {
      name: 'Multimedia obra',
      path: '/perfil/multimedia',
      component: () => import('@/views/Perfil/PerfilMultimedia')
    },
    {
      name: 'Requerimientos técnicos obra',
      path: '/perfil/requerimientos-tecnicos',
      component: () => import('@/views/Perfil/PerfilRequerimientosTecnicos')
    },
    {
      name: 'Requisitos obra',
      path: '/perfil/requisitos',
      component: () => import('@/views/Perfil/PerfilRequisitos')
    },
    {
      name: 'Editar perfil teatro/festival',
      path: '/perfil/:slug/editar',
      component: () => import('@/views/TeatroEdit')
    },
    {
      name: 'Confirmación de gira',
      path: '/ok/confirmacion',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: '¡Lo hiciste!',
        texto: 'Ya estás listo para activar tu gira',
        AccionPrincipal: 'ACTIVAR LA GIRA',
        AccionPrincipalLink: '/ok/activar-gira',
        AccionSecundaria: 'ver perfil',
        AccionSecundariaLink: '/perfil'
      }
    },
    {
      name: 'Gira activa',
      path: '/ok/activar-gira',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Ya activaste tu gira',
        texto: 'Estate atento a las propuestas de los teatros',
        AccionPrincipal: 'LISTO',
        AccionPrincipalLink: '/notificaciones',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Aceptacion de propuesta',
      path: '/ok/acuerdo',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: '¡Felicitaciones!',
        texto: '#Espectáculo tiene una nueva función en #NombreTeatroOFestival en la ciudad de #Ciudad. ',
        aclaracion: 'Una copia del acuerdo de participación se te ha enviado por mail junto con los datos de contacto del #Teatro',
        AccionPrincipal: 'VER GIRA',
        AccionPrincipalLink: '/gira',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Solicitud de modificación',
      path: '/ok/modificacion',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Tu solicitud fue enviada',
        texto: 'El teatro evaluará tu oferta. ¡Estate atento!',
        aclaracion: '',
        AccionPrincipal: 'LISTO',
        AccionPrincipalLink: '/propuestas',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Notificaciones',
      path: '/notificaciones',
      component: () => import('@/views/Notificaciones')
    },
    {
      name: 'Propuestas',
      path: '/propuestas',
      component: () => import('@/views/Propuestas')
    },
    {
      name: 'Propuesta',
      path: '/propuesta/:slug',
      component: () => import('@/views/Propuestas/PropuestasOpen')
    },
    {
      name: 'Propuesta negociacion',
      path: '/propuesta/:slug/modificacion',
      component: () => import('@/views/Negociacion')
    },
    {
      name: 'Propuesta editar',
      path: '/propuesta/:slug/editar',
      component: () => import('@/views/Propuestas/PropuestasEdit')
    },
    {
      name: 'Acuerdo de participación',
      path: '/acuerdo/:slug',
      component: () => import('@/views/Acuerdo')
    },
    {
      name: 'Gira',
      path: '/gira',
      component: () => import('@/views/Gira')
    },
    {
      name: 'Teatro',
      path: '/teatro/:slug',
      component: () => import('@/views/Teatro')
    },
    {
      name: 'Ubicacion',
      path: '/teatro/:slug/ubicacion',
      component: () => import('@/views/TeatroMapa')
    },
    {
      name: 'Sala',
      path: '/teatro/:slug/:slug',
      component: () => import('@/views/TeatroSala')
    },
    {
      name: 'Funcion',
      path: '/funcion/:slug/',
      component: () => import('@/views/FuncionOpen')
    },
    {
      name: 'Funcion',
      path: '/funcion/:slug/editar-fecha/',
      component: () => import('@/views/FuncionFechaEdit')
    },
    {
      name: 'Funcion',
      path: '/funcion/:slug/editar-sala/',
      component: () => import('@/views/FuncionSalaEdit')
    },
    {
      name: 'Registro de teatro',
      path: '/registro/teatro/',
      component: () => import('@/views/Registro/RegistroTeatro')
    },
    {
      name: 'Agregar sala',
      path: '/agregar-sala/',
      component: () => import('@/views/SalaAdd')
    },
    {
      name: 'Agregar sala',
      path: '/ok/sala',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Ya agregamos tu sala',
        texto: 'Ahora puedes acceder a la lista de espectáculos que estarán cerca',
        aclaracion: '',
        AccionPrincipal: 'VER ESPECTÁCULOS',
        AccionPrincipalLink: '/cerca',
        AccionSecundaria: 'AGREGAR OTRA SALA',
        AccionSecundariaLink: '/agregar-sala'
      }
    },
    {
      name: 'Agregar teatro',
      path: '/agregar-teatro/',
      component: () => import('@/views/TeatroAdd')
    },
    {
      name: 'Cerca',
      path: '/cerca/',
      component: () => import('@/views/Cerca'),
    },
    {
      name: 'Espectaculo Open',
      path: '/espectaculo/:slug',
      component: () => import('@/views/EspectaculoOpen')
    },
    {
      name: 'Agregar propuesta',
      path: '/agregar-propuesta',
      component: () => import('@/views/Propuestas/PropuestasAdd')
    },
    {
      name: 'Propuesta enviada',
      path: '/ok/propuesta',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Enviamos la propuesta',
        texto: '#NombreEspectaulo recibirá tu oferta. Puede aceptarla, rechazarla o solicitarte un cambio. ¡Estate atento!',
        aclaracion: '',
        AccionPrincipal: 'Listo',
        AccionPrincipalLink: '/propuestas',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Agenda',
      path: '/agenda',
      component: () => import('@/views/Agenda')
    },
    {
      name: 'Agregar funcion',
      path: '/agregar-funcion',
      component: () => import('@/views/FuncionAdd')
    },
    {
      name: 'Ajustes',
      path: '/ajustes',
      component: () => import('@/views/Ajustes')
    },
    {
      name: 'Ajustes de gira',
      path: '/ajustes/gira',
      component: () => import('@/views/AjustesGira')
    },
    {
      name: 'Mi cuenta',
      path: '/mi-cuenta',
      component: () => import('@/views/Cuenta')
    },
    {
      name: 'Mi cuenta',
      path: '/ok/ajustes',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Tus datos fueron actualizados',
        texto: '',
        aclaracion: '',
        AccionPrincipal: 'Listo',
        AccionPrincipalLink: '/ajustes',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Mi cuenta',
      path: '/ok/mi-cuenta',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Tus datos fueron actualizados',
        texto: '',
        aclaracion: '',
        AccionPrincipal: 'Listo',
        AccionPrincipalLink: '/ajustes',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Editar perfil',
      path: '/perfil/:slug/editar',
      component: () => import('@/views/TeatroEdit'),
      props: {
        mensaje: 'Tus datos fueron actualizados',
        texto: '',
        aclaracion: '',
        AccionPrincipal: 'Listo',
        AccionPrincipalLink: '/home',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Perfil edicion',
      path: '/ok/perfil',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'Tus datos fueron actualizados',
        texto: '',
        aclaracion: '',
        AccionPrincipal: 'Listo',
        AccionPrincipalLink: '/home',
        AccionSecundaria: '',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Pagos',
      path: '/pagos',
      component: () => import('@/views/Pagos')
    },
    {
      name: 'Pago confirmación',
      path: '/ok/pago',
      component: () => import('@/views/MensajeOk'),
      props: {
        mensaje: 'El pago se realizó correctamente',
        texto: 'Puedes seguir usando nuestros servicios',
        aclaracion: '',
        AccionPrincipal: 'Listo',
        AccionPrincipalLink: '/home',
        AccionSecundaria: 'Descargar comprobante',
        AccionSecundariaLink: ''
      }
    },
    {
      name: 'Pagos',
      path: '/pago/:id',
      component: () => import('@/views/Checkout')
    }
  ]

})
